import path from "path"
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import WindiCSS from 'vite-plugin-windicss';
import ElementPlus from "unplugin-element-plus/vite"
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";


export default defineConfig({
  plugins: [
    vue(),
    WindiCSS(),
    ElementPlus({
      useSource: false,
    }),
    AutoImport({
      resolvers: [ElementPlusResolver({ importStyle: "sass" })],
    }),
    Components({
      resolvers: [ElementPlusResolver({ importStyle: "sass" })],
    }),
  ],
  resolve: {
    alias: [{ find: "@", replacement: path.resolve(__dirname, "src/") }],
},
});
