import { useRoute } from 'vue-router'

type Object = {
    [key: string]: string;
};

export const filter = (obj: Object, keyword: string  = "") => {
    return Object.keys(obj)
        .filter(key => key.toLowerCase().includes(keyword.trim().toLowerCase()))
        .reduce((res: Object, key) => (res[key] = obj[key], res), {});
}

export const url = (dir: string | number, answer: string  = "") => {
    return `./src/images/part/one/${dir}/${answer}.jpg`
};

export const urlPartTwo = (dir: string | number, answer: string  = "") => {
    return `./src/images/part/two/${dir}/${answer}.jpg`
};

export const urlPart = (dir: string | number, answer: string  = "") => {
    const route = useRoute()
    return `/src/images${route.path}/${dir}/${answer}.jpg`
};

export const urlList = (dir: string | number, answer: string  = "") => {
    const route = useRoute()
    return `/src/images${route.path}/${dir}/${answer}.jpg`
};


export const alphabet = Array.from({ length: 26 }, (_, i) => String.fromCharCode(97 + i));

