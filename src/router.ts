import { createWebHistory, createRouter } from 'vue-router'

import One from './part/one.vue'
import Two from './part/two.vue'
import Three from './part/three.vue'
import Four from './part/four.vue'
import Five from './part/five.vue'
import Six from './part/six.vue'
import Seven from './part/seven.vue'
import Eight from './part/eight.vue'
import Nine from './part/nine.vue'

import listenOne from './listen/one.vue'
import listenTwo from './listen/two.vue'
import listenThree from './listen/three.vue'

const routes = [
  { path: '/', component: One,},
  { path: '/one', component: One,},
  { path: '/two', component: Two },
  { path: '/three', component: Three },
  { path: '/four', component: Four },
  { path: '/five', component: Five },
  { path: '/six', component: Six },
  { path: '/seven', component: Seven },
  { path: '/eight', component: Eight },
  { path: '/nine', component: Nine },
  { path: '/listen/one', component: listenOne },
  { path: '/listen/two', component: listenTwo },
  { path: '/listen/three', component: listenThree },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router