import { defineConfig } from "windicss/helpers";

export default defineConfig({
  preflight: true,
  attributify: {
    prefix: 'tw:',
  },
  extract: {
    include: ["src/**/*.{vue,html,ts,tsx}"],
    exclude: ["node_modules", ".git"],
  },
});
